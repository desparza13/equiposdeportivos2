import { Component, OnInit } from '@angular/core';
import { Equipos } from '../models/equipos';
import { EquiposService } from '../services/equipos.service';
import { Jugadores } from '../models/jugadores';
import { JugadoresService } from '../services/jugadores.service';
import { Resultados } from '../models/resultados';
import { ResultadosService } from '../services/resultados.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  equipos: Equipos[];
  jugadores: Jugadores[];
  resultados: Resultados[];
  constructor(private equiposService: EquiposService, private jugadoresService: JugadoresService, private resultadosService: ResultadosService) {}
  ngOnInit() {
    this.equiposService.getAllEquipos().subscribe(res => {
      console.log('Equipos', res);
      this.equipos = res;
    });
    this.jugadoresService.getAllJugadores().subscribe(res => {
      console.log('Jugadores', res);
      this.jugadores = res;
    });
    this.resultadosService.getAllResultados().subscribe(res => {
      console.log('Resultados', res);
      this.resultados = res;
    });
  }


}
