export interface Jugadores {
    id?:string;
    apellidos:string;
    equipoID:string;
    foto:string;
    nombre:string;
}
