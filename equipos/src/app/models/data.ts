export interface Data {
    id?:string;
    fotosJugadores:Array<string>;
    jugadores:Array<string>;
    nombre:string;
    logo:string;
}
