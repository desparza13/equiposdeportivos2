import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Jugadores} from '../models/jugadores';

@Injectable({
  providedIn: 'root'
})
export class JugadoresService {
  private jugadoresCollection : AngularFirestoreCollection<Jugadores>;
  private jugadores : Observable<Jugadores[]>;

  constructor(db: AngularFirestore) {
    this.jugadoresCollection = db.collection<Jugadores>('jugadores');
    this.jugadores = this.jugadoresCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map( a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data}
        });
      }
    ))
  }
  getAllJugadores() {
    return this.jugadores;
  }
  getJugadores(id:string) {
    return this.jugadoresCollection.doc<Jugadores>(id).valueChanges();
  }  
  updateJugadores(jugadores:Jugadores, id:string)
  {
    return this.jugadoresCollection.doc(id).update(jugadores);
  }
}
