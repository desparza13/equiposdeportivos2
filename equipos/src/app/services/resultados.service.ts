import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Resultados} from '../models/resultados';

@Injectable({
  providedIn: 'root'
})
export class ResultadosService {
  private resultadosCollection : AngularFirestoreCollection<Resultados>;
  private resultados : Observable<Resultados[]>;

  constructor(db: AngularFirestore) {
    this.resultadosCollection = db.collection<Resultados>('resultados');
    this.resultados = this.resultadosCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map( a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data}
        });
      }
    ))
  }
  getAllResultados() {
    return this.resultados;
  }
  getResultados(id:string) {
    return this.resultadosCollection.doc<Resultados>(id).valueChanges();
  }  
  updateResultados(resultados:Resultados, id:string)
  {
    return this.resultadosCollection.doc(id).update(resultados);
  }
}

